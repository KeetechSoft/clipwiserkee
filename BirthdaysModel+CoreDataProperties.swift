//
//  BirthdaysModel+CoreDataProperties.swift
//  
//
//  Created by Development on 4/14/16.
//
//
//  Choose "Create NSManagedObject Subclass…" from the Core Data editor menu
//  to delete and recreate this implementation file for your updated model.
//

import Foundation
import CoreData

extension BirthdaysModel {

    @NSManaged var uuid: String?
    @NSManaged var dateOfBirth: String?
    @NSManaged var targetDate: String?
    @NSManaged var nextBirthdayDate: String?
    @NSManaged var age: String?
    @NSManaged var years: String?
    @NSManaged var months: String?
    @NSManaged var weeks: String?
    @NSManaged var days: String?
    @NSManaged var hours: String?
    @NSManaged var minutes: String?
    @NSManaged var seconds: String?
    @NSManaged var name: String?
    @NSManaged var savedDate: String?
    @NSManaged var checked: NSNumber?

}

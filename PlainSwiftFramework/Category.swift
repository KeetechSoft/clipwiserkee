//
//  Category.swift
//  PlainSwiftFramework
//
//  Created by mahesh on 07/10/16.
//  Copyright © 2016 maheshbabu.somineni. All rights reserved.
//

import UIKit

class Category: NSObject {

    let category_id: Int
    let created_at: String
    let display_name: String
    let image: String
    let name: String
    let updated_at: String

    init(category_id: Int, created_at: String, display_name: String, image:String, name:String, updated_at:String ) {
        
        self.category_id = category_id
        self.created_at = created_at
        self.display_name = display_name
        self.image = image
        self.name = name
        self.updated_at = updated_at
    }
}

//
//  Constants.swift
//  GameFramework
//
//  Created by apple on 1/16/15.
//  Copyright (c) 2015 apple. All rights reserved.
//

import Foundation

struct GlobalVariables {
    static let globalUserDefaults = NSUserDefaults.standardUserDefaults()
    static let globalSingleton =  GlobalSingleton.sharedInstance
    static let blue_color = "#4AB3EF"
    static let green_color = "#fc4f02"
    static let yellow_color = "#fc4f02"
    static let appColor = "#FECB4D"
    static let timeInterval = 60.0
    static let admobBannerId = "ca-app-pub-1963486029431498/4739617964"
    static let admobInterstialId = "ca-app-pub-1963486029431498/6216351167"
    static let isPurchased : Bool = true
    static let timeFormat = "yyyy-MM-dd"
    static let inappPurchaseProductId = "com.sptit.agecalculator.adsremove"
   
    //static let inappPurchaseProductId = "com.sptcs.everynote"
    static let iPhone5 = UIScreen.mainScreen().bounds.size.height == 568
    static let iPad = UIScreen.mainScreen().bounds.size.height == 1024
    static let iPhone4 = UIScreen.mainScreen().bounds.size.height == 480

    static let appName = "Age Calculator Pro"
  
    //Service calls values
    static let request_url = "http://clipwiser.duoex.com/api/"
    static let client_id = "1234"
    static let device_id = "355004"
    static let client_secret = "abcd"
    static let moreAppsURL = "https://itunes.apple.com/in/developer/sptit.com/id1082253495"
    
    //static let appReviewURL =  "https://itunes.apple.com/us/app/rugby-scoreboard/id507755804?mt=8"
    static let appReviewURL =  "https://itunes.apple.com/in/developer/sptit.com/id1082253495"
    static let x_parse_application_id_key = "X-Parse-Application-Id"
    static let authorization = "Authorization"
    static let x_parse_rest_api_key = "X-Parse-REST-API-Key"
    static let x_parse_rest_api_value = "PF90VEIV03MedSVp3SVz2Q75QO0SsUZWW2RC09WS"
    static let x_parse_client_key_value = "AxXgRnQRxorVr15m8RLCkLsDwztJMO7raAr9DDLD"
    static let request_content_type_key = "Content-Type"
    static let request_content_type_value = "application/x-www-form-urlencoded"
    static let request_content_length_key = "Content-Length"
    static let request_type_value = "POST"

    //https://projects.invisionapp.com/m/share/8P8JSBPUJ#/191475582

    //User defuals keys
    static let show_introduction_view_key = "show_introduction_view_key"
    static let access_token_key = "access_token"
    static let user_defaults_app_iteration_number_key = "app_iteration_number"
    static let user_defaults_isPurchased_key = "isPurchased"
    static let user_defaults_deviceid_key = "deviceid"

    
    //Global enums
    enum RequestAPIMethods : NSString{
        case get­token = "auth/get-token"
        case register = "user/register"
        case login = "user/login"
        case getCategories = "category/all"
        case getProducts = "category"
    }
    enum CoreDataEntities : NSString{
        case Movies = "MoviesModel"
        case ReviewOrComment = "CommentsModel"
        case NotesModel = "NotesModel"
        case BirthdaysModel = "BirthdaysModel"
    }
    enum BookmarkType : NSString{
        case Institute = "Institute"
        case Trainer = "Trainer"
        case Course = "Course"
    }
    enum SlideMenuItem : Int{
        case HOME = 0
        case INSTITUTES = 1
        case COURSES = 2
        case ACCOUNT = 3
    }
   
}


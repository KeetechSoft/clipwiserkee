//
//  Animal.swift
//  SlideOutNavigation
//
//  Created by James Frost on 03/08/2014.
//  Copyright (c) 2014 James Frost. All rights reserved.
//

import UIKit


class MenuItem {
  
  let title: String
  let creator: String
  let image: UIImage?
  
  init(title: String, creator: String, image: UIImage?) {
    
    self.title = title
    self.creator = creator
    self.image = image

  }
  
  class func getAllMenuItems() -> Array<MenuItem> {
    
        return [ MenuItem(title: "Calculator", creator: "address", image: UIImage(named: "age-menu-item.png")), MenuItem(title: "Birthdays", creator: "address", image: UIImage(named: "birthdays-menu-item.png"))]

    }
  
}
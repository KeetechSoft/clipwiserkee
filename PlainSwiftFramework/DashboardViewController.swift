//
//  DashboardViewController.swift
//  PlainSwiftFramework
//
//  Created by maheshbabu.somineni on 9/27/16.
//  Copyright © 2016 maheshbabu.somineni. All rights reserved.
//

import UIKit

class DashboardViewController: BaseViewController {
    
    let spaceValue:Int = 2
    @IBOutlet weak var dashboardCollectionView: UICollectionView!
    var categoriesArray:NSMutableArray = NSMutableArray()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // Do any additional setup after loading the view.
        configureView()
        getCategoriesServiceCall()
        
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
        
    }
    func configureView(){
        dashboardCollectionView.backgroundColor = UIColor.clearColor()
    }
    
    /*
     // MARK: - Navigation
     
     // In a storyboard-based application, you will often want to do a little preparation before navigation
     override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
     // Get the new view controller using segue.destinationViewController.
     // Pass the selected object to the new view controller.
     }
     */
    // MARK: - Service Calls
    func getCategoriesServiceCall(){
        
        super.showProgress()
        //Save to defaults
        let params:NSDictionary = ["device_id": GlobalVariables.device_id]
        print(params)
        NetworkManager.getRequest(params as! [String : String],requestMethod: GlobalVariables.RequestAPIMethods.getCategories,  delegate: self)
        
    }
    
    // MARK: Service response handlers
    override func dataDelegate(responseData: AnyObject, requestMethod: GlobalVariables.RequestAPIMethods) {
        super.dataDelegate(responseData, requestMethod: requestMethod)
        let jsonCategoriesArray = responseData["all_categories"] as! [AnyObject]
        
        for dictionary in jsonCategoriesArray {
            let category = Category(category_id: dictionary["category_id"] as! Int, created_at: dictionary["created_at"]as! String, display_name: dictionary["display_name"] as! String, image: dictionary["image"] as! String, name: dictionary["name"] as! String, updated_at: dictionary["updated_at"] as! String)
            
            categoriesArray.addObject(category)
        }
        
        self.dashboardCollectionView.reloadData()
    }
    
}
extension DashboardViewController:UICollectionViewDelegate,UICollectionViewDataSource, UICollectionViewDelegateFlowLayout{
    func numberOfSectionsInCollectionView(collectionView: UICollectionView) -> Int {
        return 1
    }
    func collectionView(collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return categoriesArray.count
    }
    func collectionView(collectionView: UICollectionView, cellForItemAtIndexPath indexPath: NSIndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCellWithReuseIdentifier("Cell", forIndexPath: indexPath) as UICollectionViewCell
        
        let imageView = cell.viewWithTag(1000) as! UIImageView
        let lable = cell.viewWithTag(1001) as! UILabel
        
        let object:Category = categoriesArray.objectAtIndex(indexPath.row) as! Category
        imageView.sd_setImageWithURL(NSURL(string: object.image))
        lable.text =  object.display_name
        
        return cell
    }
    func collectionView(collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, insetForSectionAtIndex section: Int) -> UIEdgeInsets {
        return UIEdgeInsetsMake(CGFloat(spaceValue), CGFloat(spaceValue), CGFloat(spaceValue), CGFloat(spaceValue))
        
    }
    func collectionView(collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAtIndexPath indexPath: NSIndexPath) -> CGSize {
        return  CGSizeMake(self.view.bounds.width/3 - 2*CGFloat(spaceValue), self.view.bounds.width/3 - 2*CGFloat(spaceValue))
    }
    func collectionView(collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumInteritemSpacingForSectionAtIndex section: Int) -> CGFloat {
        return CGFloat(spaceValue)
    }
    func collectionView(collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumLineSpacingForSectionAtIndex section: Int) -> CGFloat {
        return CGFloat(spaceValue)
    }
    func collectionView(collectionView: UICollectionView, didSelectItemAtIndexPath indexPath: NSIndexPath) {
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        let destinationViewController:PhotoStreamViewController = storyboard.instantiateViewControllerWithIdentifier("PhotoStreamViewController") as! PhotoStreamViewController
        self.navigationController?.pushViewController(destinationViewController, animated: true)
    }
}

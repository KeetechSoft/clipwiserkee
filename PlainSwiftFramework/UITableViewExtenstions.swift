//
//  UITableViewExtenstions.swift
//  PlainSwiftFramework
//
//  Created by mahesh on 13/04/16.
//  Copyright © 2016 maheshbabu.somineni. All rights reserved.
//

import Foundation
extension UITableView {
    override func addBackground(imageName:String) {
        
        // screen width and height:
        let imageViewBackground = UIImageView(frame: CGRectMake(0, 0, UIScreen.mainScreen().bounds.size.width, UIScreen.mainScreen().bounds.size.height))
        imageViewBackground.image = UIImage(named:imageName)
        
        // you can change the content mode:
        imageViewBackground.contentMode = UIViewContentMode.ScaleAspectFill
        
        self.addSubview(imageViewBackground)
        self.sendSubviewToBack(imageViewBackground)
    }
    func addBackgroundBorder() {
        
        
        
        // screen width and height:
        let imageViewBackground = UIImageView(frame: CGRectMake(5, 2, UIScreen.mainScreen().bounds.size.width-10, UIScreen.mainScreen().bounds.size.height - 70))
        //imageViewBackground.image = UIImage(named:imageName)
        imageViewBackground.backgroundColor = UIColor.clearColor()
        
        // you can change the content mode:
        imageViewBackground.contentMode = UIViewContentMode.ScaleAspectFill
        imageViewBackground.layer.cornerRadius = 8
        //imageViewBackground.layer.borderColor = UIColor(rgba: GlobalVariables.appColor).CGColor
        imageViewBackground.layer.borderWidth = 3.0
        
        self.addSubview(imageViewBackground)
        self.sendSubviewToBack(imageViewBackground)
    }
}
//
//  SignupViewController.swift
//  PlainSwiftFramework
//
//  Created by maheshbabu.somineni on 9/26/16.
//  Copyright © 2016 maheshbabu.somineni. All rights reserved.
//

import UIKit

class SignupViewController: BaseViewController,UIScrollViewDelegate {

    @IBOutlet var textfiledsCollectionArray: [UITextField]!
    @IBOutlet weak var registrationScrollView: UIScrollView!
  

    enum TagsEnum : Int {
        case FirstNameTag = 1000
        case LastNameTag = 1001
        case EmailTag = 1002
        case PasswordTag = 1003
        case MobileTag = 1004
        case DobTag = 1005
        case GenderTag = 1006
        case DOBArrowButtonTag = 1007
        case GenderArrowButtonTag = 1008
        case DatePickerTag = 1009
        case GenderPickerTag = 1010
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        configureView()
    }
    func configureView(){
        //Add left view to textfileds
        let dobTextField:UITextField = getElementFromCollection(TagsEnum.DobTag.rawValue)
        let genderTextField:UITextField = getElementFromCollection(TagsEnum.GenderTag.rawValue)
        dobTextField.rightView = self.createArrowButton(TagsEnum.DOBArrowButtonTag.rawValue)
        dobTextField.rightViewMode = UITextFieldViewMode.Always
        genderTextField.rightView = self.createArrowButton(TagsEnum.GenderArrowButtonTag.rawValue)
        genderTextField.rightViewMode = UITextFieldViewMode.Always

    }
    func createArrowButton(aTag:Int) -> UIButton{
        
        let button = UIButton(type: .Custom)
        button.frame = CGRectMake(0, 0, 28, 28)
        button.setImage(UIImage(named: "arrow.png"), forState: .Normal)
        button.addTarget(self, action: #selector(self.arrowButtonClick), forControlEvents: UIControlEvents.TouchUpInside)
        button.tag = aTag
        return button
    }

    func scrollViewDidScroll(scrollView: UIScrollView) {
        scrollView.contentOffset.x = 0.0
    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

    // MARK: - Button click events
    @IBAction func signinButtonClick(sender: AnyObject) {
        self.navigationController?.popViewControllerAnimated(true)
        
    }
    @IBAction func signupButtonClick(sender: AnyObject) {
        
        let firstnameTextField:UITextField = getElementFromCollection(TagsEnum.FirstNameTag.rawValue)
        let lastnameTextField:UITextField = getElementFromCollection(TagsEnum.LastNameTag.rawValue)
        let emailTextField:UITextField = getElementFromCollection(TagsEnum.EmailTag.rawValue)
        let passwordTextField:UITextField = getElementFromCollection(TagsEnum.PasswordTag.rawValue)
        let mobileTextField:UITextField = getElementFromCollection(TagsEnum.MobileTag.rawValue)
        let dobTextField:UITextField = getElementFromCollection(TagsEnum.DobTag.rawValue)
        let genderTextField:UITextField = getElementFromCollection(TagsEnum.GenderTag.rawValue)
        
        if(emailTextField.text == ""){
            let alert = UIAlertController(title:GlobalVariables.appName, message: "Please enter valid email id and password.", preferredStyle: UIAlertControllerStyle.Alert)
            alert.addAction(UIAlertAction(title: "Ok", style: UIAlertActionStyle.Default, handler: nil))
            self.presentViewController(alert, animated: true, completion: nil)
        }else if(passwordTextField.text == ""){
            
            let alert = UIAlertController(title:GlobalVariables.appName, message: "Please enter valid email id and password.", preferredStyle: UIAlertControllerStyle.Alert)
            alert.addAction(UIAlertAction(title: "Ok", style: UIAlertActionStyle.Default, handler: nil))
            self.presentViewController(alert, animated: true, completion: nil)
        }else{
            
            let params:NSDictionary = ["device_id": GlobalVariables.device_id,"email": emailTextField.text!,"password":passwordTextField.text!,"fname":firstnameTextField.text!,"lname":lastnameTextField.text!,"mobile":mobileTextField.text!,"dob":dobTextField.text!,"gender":genderTextField.text!]
            
            self.signupServiceCall(params)
        }

    }
    @IBAction func remembermeButtonClick(sender: AnyObject) {
    }
    func arrowButtonClick(sender:AnyObject){
        
        let button = sender as! UIButton
        if button.tag == TagsEnum.DOBArrowButtonTag.rawValue{
            self.showDatePickerView(TagsEnum.DatePickerTag.rawValue)
        }else{
            self.showGenderPickerView(TagsEnum.GenderPickerTag.rawValue)
        }
        
    }
    // MARK: - Custom methods
    func getElementFromCollection(tag:Int) -> UITextField{
        
        for textField in textfiledsCollectionArray {
            
            if textField.tag == tag{
                return textField
            }
        }
        return UITextField()
    }
    // MARK: - Service methods
    func signupServiceCall(params:NSDictionary){
        
        super.showProgress()
        //Save to defaults
       print(params)
        NetworkManager.postRequest(params as! [String : String],requestMethod: GlobalVariables.RequestAPIMethods.register,  delegate: self)
        
    }

    // MARK: Service response handlers
    override func dataDelegate(responseData: AnyObject, requestMethod: GlobalVariables.RequestAPIMethods) {
        super.dataDelegate(responseData, requestMethod: requestMethod)
        
        //let responseDictionary = responseData as! [String:AnyObject]
        self.navigationController?.popViewControllerAnimated(true)
        
    }
    //MARK: Picker methods
    func showDatePickerView(pickerViewTag:Int){
        var datePickerView: UIDatePicker!
        
        //Creating pickerview object
        if(UIDevice.currentDevice().userInterfaceIdiom == .Pad){
            datePickerView = UIDatePicker(frame: CGRectMake(-225, 0, self.view.frame.size.width, 200))
        }else{
            datePickerView = UIDatePicker(frame: CGRectMake(-10, 0, self.view.frame.size.width, 200))
        }
        datePickerView.datePickerMode = UIDatePickerMode.Date
        
        //Creating view to add datepicker
        let viewPicker: UIView = UIView(frame: CGRectMake(0, 0, self.view.frame.size.width, 200))
        viewPicker.backgroundColor = UIColor.clearColor()
        
        //Initializing date picker
        datePickerView.tag = pickerViewTag
        viewPicker.addSubview(datePickerView)
        
        let alertController = UIAlertController(title: GlobalVariables.appName, message:"\n\n\n\n\n\n\n\n\n", preferredStyle: .ActionSheet)
        alertController.view.addSubview(viewPicker)
        
        let cancelAction = UIAlertAction(title: "Cancel", style: .Cancel) { (action) in
            
            // ...
        }
        alertController.addAction(cancelAction)
        
        let OKAction = UIAlertAction(title: "OK", style: .Default) { (action) in
            
            let dateString =  GlobalSettings.getDateString(datePickerView.date)
            let dobTextField:UITextField = self.getElementFromCollection(TagsEnum.DobTag.rawValue)
            dobTextField.text = dateString
            //Update note
            //self.refreshView()
        }
        alertController.addAction(OKAction)
        alertController.popoverPresentationController?.sourceView = self.view
        alertController.popoverPresentationController?.sourceRect = CGRectMake(self.view.bounds.width / 2.0, self.view.bounds.height / 2.0, 1.0, 1.0) // this is the center of the screen currently but it can be any point in the view
        alertController.popoverPresentationController?.permittedArrowDirections = UIPopoverArrowDirection.Down
        self.presentViewController(alertController, animated: true, completion: nil)
    }
    func showGenderPickerView(pickerViewTag:Int){
        
        var pickerView: UIPickerView!
        
        //Creating pickerview object
        if(UIDevice.currentDevice().userInterfaceIdiom == .Pad){
            pickerView = UIPickerView(frame: CGRectMake(-225, 0, self.view.frame.size.width, 200))
        }else{
            pickerView = UIPickerView(frame: CGRectMake(-10, 0, self.view.frame.size.width, 200))
        }
        pickerView.dataSource = self
        pickerView.delegate = self
        
        //Creating view to add datepicker
        let viewPicker: UIView = UIView(frame: CGRectMake(0, 0, self.view.frame.size.width, 200))
        viewPicker.backgroundColor = UIColor.clearColor()
        
        //Initializing date picker
        pickerView.tag = pickerViewTag
        viewPicker.addSubview(pickerView)
        
        let alertController = UIAlertController(title: GlobalVariables.appName, message:"\n\n\n\n\n\n\n\n\n", preferredStyle: .ActionSheet)
        alertController.view.addSubview(viewPicker)
        
        let cancelAction = UIAlertAction(title: "Cancel", style: .Cancel) { (action) in
            
            // ...
        }
        alertController.addAction(cancelAction)
        
        let OKAction = UIAlertAction(title: "OK", style: .Default) { (action) in
            
            let genderTextField:UITextField = self.getElementFromCollection(TagsEnum.GenderTag.rawValue)
            let row = pickerView.selectedRowInComponent(0)
            
            if row == 0{
                genderTextField.text = "M"
            }else{
                genderTextField.text = "F"
            }
            //Update note
            //self.refreshView()
        }
        alertController.addAction(OKAction)
        alertController.popoverPresentationController?.sourceView = self.view
        alertController.popoverPresentationController?.sourceRect = CGRectMake(self.view.bounds.width / 2.0, self.view.bounds.height / 2.0, 1.0, 1.0) // this is the center of the screen currently but it can be any point in the view
        alertController.popoverPresentationController?.permittedArrowDirections = UIPopoverArrowDirection.Down
        self.presentViewController(alertController, animated: true, completion: nil)
    }

}
extension SignupViewController:UIPickerViewDelegate,UIPickerViewDataSource{
    func numberOfComponentsInPickerView(pickerView: UIPickerView) -> Int {
        return 1
    }
    func pickerView(pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        return 2
    }
    func pickerView(pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
        if row == 0{
            return "Male"
        }else{
            return "Female"
        }
    }
    
}

//
//  DateExtension.swift
//  PlainSwiftFramework
//
//  Created by maheshbabu.somineni on 4/7/16.
//  Copyright © 2016 maheshbabu.somineni. All rights reserved.
//

import Foundation
extension NSDate {
    func ageDifference(date:NSDate) -> NSDateComponents{
        let age: NSCalendarUnit = [.Year, .Month, .Day]
        let difference = NSCalendar.currentCalendar().components(age, fromDate: date, toDate: self, options: [])
        return difference
    }
    func nextBirthday(date:NSDate) -> NSDateComponents{
        
        let calendar = NSCalendar.currentCalendar()
        let todayDateComponents = calendar.components([.Day , .Month , .Year], fromDate: self)
        let todayMonth = todayDateComponents.month
        let currentYear = todayDateComponents.year

        let birthDateComponents = calendar.components([.Day , .Month , .Year], fromDate: date)
        let birthDayMonth = birthDateComponents.month
        let day = birthDateComponents.day
        
        var nextBirthdayString = ""
        
        if todayMonth > birthDayMonth{
            nextBirthdayString = "\(currentYear+1)-\(birthDayMonth)-\(day)"
        }else{
            nextBirthdayString = "\(currentYear)-\(birthDayMonth)-\(day)"
        }
        
        let dateFormatter = NSDateFormatter()
        dateFormatter.dateFormat = GlobalVariables.timeFormat
        
        let nextBirthdayDate = dateFormatter.dateFromString(nextBirthdayString)
        
        let age: NSCalendarUnit = [.Year, .Month, .Day]
        let difference = NSCalendar.currentCalendar().components(age, fromDate: self, toDate: nextBirthdayDate!, options: [])
        return difference
    }
    func yearsFrom(date:NSDate) -> Int{
        return NSCalendar.currentCalendar().components(.Year, fromDate: date, toDate: self, options: []).year
    }
    func monthsFrom(date:NSDate) -> Int{
        return NSCalendar.currentCalendar().components(.Month, fromDate: date, toDate: self, options: []).month
    }
    func weeksFrom(date:NSDate) -> Int{
        return NSCalendar.currentCalendar().components(.WeekOfYear, fromDate: date, toDate: self, options: []).weekOfYear
    }
    func daysFrom(date:NSDate) -> Int{
        return NSCalendar.currentCalendar().components(.Day, fromDate: date, toDate: self, options: []).day
    }
    func hoursFrom(date:NSDate) -> Int{
        return NSCalendar.currentCalendar().components(.Hour, fromDate: date, toDate: self, options: []).hour
    }
    func minutesFrom(date:NSDate) -> Int{
        return NSCalendar.currentCalendar().components(.Minute, fromDate: date, toDate: self, options: []).minute
    }
    func secondsFrom(date:NSDate) -> Int{
        return NSCalendar.currentCalendar().components(.Second, fromDate: date, toDate: self, options: []).second
    }
    func offsetFrom(date:NSDate, calculator:Calculator) -> Calculator {
        
        var car = calculator
        car.age = "\(ageDifference(date).year)Y \(ageDifference(date).month)M \(ageDifference(date).day)D"
        car.years = "\(yearsFrom(date))y"
        car.months = "\(monthsFrom(date))M"
        car.weeks = "\(weeksFrom(date))w"
        car.days = "\(daysFrom(date))d"
        car.hours = "\(hoursFrom(date))h"
        car.minutes = "\(minutesFrom(date))m"
        car.seconds = "\(secondsFrom(date))s"
        car.nextBirthdayDate = "\(nextBirthday(date).year)Y \(nextBirthday(date).month)M \(nextBirthday(date).day)D"
        return car
    }
   
}
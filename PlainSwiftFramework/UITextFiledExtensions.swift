//
//  UITextFiledExtensions.swift
//  PlainSwiftFramework
//
//  Created by mahesh on 13/04/16.
//  Copyright © 2016 maheshbabu.somineni. All rights reserved.
//

import Foundation

extension UITextField {
    func addRightView(imageName:String) {
        
        let rightview = UIView()
        let image = UIImage(named:imageName) as UIImage?
        rightview.frame = CGRectMake(0, 0, 35, 25)
        let button = UIButton(frame :CGRect(x: 0, y: 0, width: 28, height: 22))
        button.setBackgroundImage(image, forState: .Normal)
        rightview.addSubview(button)
        self.rightView = rightview;
        self.rightViewMode = UITextFieldViewMode.Always
       
    }
}
   
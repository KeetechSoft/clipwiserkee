//
//  LoginViewController.swift
//  PlainSwiftFramework
//
//  Created by maheshbabu.somineni on 9/20/16.
//  Copyright © 2016 maheshbabu.somineni. All rights reserved.
//

import UIKit

class LoginViewController: BaseViewController {

    @IBOutlet weak var horizantalStackView: UIStackView!
    @IBOutlet weak var useranmeTextFiled: UITextField!
    @IBOutlet weak var passwordTextFiled: UITextField!
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        configureView()
        getAccessTokenServiceCall()

        
    }

    func configureView(){
        self.navigationController?.navigationBarHidden = true
        self.view.bringSubviewToFront(horizantalStackView)
    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */
    
    // MARK: - Button actions
    @IBAction func loginButtonClick(sender: AnyObject) {
        if(self.useranmeTextFiled.text == ""){
            let alert = UIAlertController(title:GlobalVariables.appName, message: "Please enter valid email id and password.", preferredStyle: UIAlertControllerStyle.Alert)
            alert.addAction(UIAlertAction(title: "Ok", style: UIAlertActionStyle.Default, handler: nil))
            self.presentViewController(alert, animated: true, completion: nil)
        }else if(self.passwordTextFiled.text == ""){
            let alert = UIAlertController(title:GlobalVariables.appName, message: "Please enter valid email id and password.", preferredStyle: UIAlertControllerStyle.Alert)
            alert.addAction(UIAlertAction(title: "Ok", style: UIAlertActionStyle.Default, handler: nil))
            self.presentViewController(alert, animated: true, completion: nil)
        }else{
            self.loginServiceCall()
        }
    }
    @IBAction func forgotPasswordButtonClick(sender: AnyObject) {
    }
    @IBAction func createAccountButtonClick(sender: AnyObject) {
    }
    @IBAction func facebookButtonClick(sender: AnyObject) {
    }
    
    // MARK: - Service Calls
    func loginServiceCall(){
        
        super.showProgress()
        //Save to defaults
        let params:NSDictionary = ["device_id": GlobalVariables.device_id,"email": self.useranmeTextFiled.text!,"password":self.passwordTextFiled.text!]
        print(params)
        NetworkManager.postRequest(params as! [String : String],requestMethod: GlobalVariables.RequestAPIMethods.login,  delegate: self)

    }
    
    func getAccessTokenServiceCall(){
        
        super.showProgress()
        
        let params:NSDictionary = ["client_id": GlobalVariables.client_id, "client_secret": GlobalVariables.client_secret ,"device_id":GlobalVariables.device_id ]
        NetworkManager.getRequest(params as! [String : AnyObject],requestMethod:  GlobalVariables.RequestAPIMethods.get­token, delegate: self)
    }
    // MARK: Service response handlers
    override func dataDelegate(responseData: AnyObject, requestMethod: GlobalVariables.RequestAPIMethods) {
        super.dataDelegate(responseData, requestMethod: requestMethod)
        
        if requestMethod.rawValue == GlobalVariables.RequestAPIMethods.login.rawValue{
            let mainStoryboard: UIStoryboard = UIStoryboard(name: "Main", bundle: nil)
            let viewController = mainStoryboard.instantiateViewControllerWithIdentifier("TabBarController")
            UIApplication.sharedApplication().keyWindow?.rootViewController = viewController;
        }else if requestMethod.rawValue == GlobalVariables.RequestAPIMethods.get­token.rawValue{
            let responseDictionary = responseData as! [String:AnyObject]
            GlobalSettings.updateUserDefaultValue(GlobalVariables.access_token_key, value:responseDictionary["access_token"] as! String)
        }
    }

}

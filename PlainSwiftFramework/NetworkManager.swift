//
//  NetworkManager.swift
//  SwiftCoreDataSimpleDemo
//
//  Created by maheshbabu.somineni on 12/10/15.
//  Copyright © 2015 CHENHAO. All rights reserved.
//

import UIKit
import Alamofire

//@objc
protocol CMNetworkDelegate{
    
    func dataDelegate(reponseData:AnyObject, requestMethod:GlobalVariables.RequestAPIMethods)
    func networkError(errorMessage:String)
}

class NetworkManager: NSObject {
    class func postRequest(parameters:[String:String],requestMethod:GlobalVariables.RequestAPIMethods, delegate:CMNetworkDelegate) {
        
        //Url object creation
        let url = NSURL(string: GlobalVariables.request_url + ((requestMethod.rawValue) as String))
        Alamofire.upload(.POST,url!,
                         headers: [GlobalVariables.authorization: GlobalSettings.getUserDefaultValue(GlobalVariables.access_token_key)],
                         multipartFormData: { multipartFormData in
                            for (key, value) in parameters {
                                multipartFormData.appendBodyPart(data: "\(value)".dataUsingEncoding(NSUTF8StringEncoding, allowLossyConversion: false)!, name :"\(key)")
                            }
            },encodingCompletion: { encodingResult in
                switch encodingResult {
                case .Success(let upload, _, _):
                    
                    upload.validate()
                    upload.responseJSON { response in
                        
                        let responseDictionary = NetworkManager.nsdataToJSON(response.data!)
                        if responseDictionary!["success"] as! String == "false"{
                            print(responseDictionary!["msg"] as! String)
                            delegate.networkError(responseDictionary!["msg"] as! String)
                        }else{
                            delegate.dataDelegate(responseDictionary!, requestMethod:requestMethod)
                        }
                    }
                    break
                case .Failure( _): break
                    delegate.networkError("Cannot load data from server!!")
                }
            }
        )
    }
    
    class func getRequest(parameters:[String:AnyObject], requestMethod:GlobalVariables.RequestAPIMethods,delegate:CMNetworkDelegate) {
        
        //Url object creation
        let url = NSURL(string: GlobalVariables.request_url + ((requestMethod.rawValue) as String) +  NetworkManager.getParamsURL(parameters))
        let request = NSMutableURLRequest(URL:url!)
        
        //Conver params to json data
        request.HTTPMethod = "GET"
        
        request.addValue(GlobalSettings.getUserDefaultValue(GlobalVariables.access_token_key), forHTTPHeaderField: GlobalVariables.authorization)
        
        //Start requesting
        let task = NSURLSession.sharedSession().dataTaskWithRequest(request) {
            data, response, error in
            
            //Connection failed case
            if error != nil {
                dispatch_async(dispatch_get_main_queue(), {
                    if error!.domain == NSURLErrorDomain && error!.code == NSURLErrorNotConnectedToInternet {
                        delegate.networkError("Not connected to Internet!!")
                    }else{
                        delegate.networkError("Cannot connect to server!!")
                    }
                })
            }else{
                do{
                    let jsonResult: AnyObject = (try NSJSONSerialization.JSONObjectWithData(data!, options: NSJSONReadingOptions.MutableContainers))
                    dispatch_async(dispatch_get_main_queue(), {
                        let responseDictionary = jsonResult as! [String:AnyObject]
                        if responseDictionary["success"] as! String == "false"{
                            print(responseDictionary["msg"] as! String)
                            delegate.networkError(responseDictionary["msg"] as! String)
                        }else{
                            delegate.dataDelegate(jsonResult, requestMethod:requestMethod)
                        }
                    })
                }catch{
                    dispatch_async(dispatch_get_main_queue(), {
                        delegate.networkError("Cannot load data from server!!")
                    })
                }
            }
        }
        task.resume()
    }
    
    //Convert dictionary to json
    class func getParamsURL(parameters: [String : AnyObject]) -> String {
        
        var getURL:String = "?"
        for (key, value) in parameters {
            if key != "method"{
                getURL = getURL + "\(key)" + "=" + "\(value)" + "&"
            }
        }
        return getURL.substringToIndex(getURL.endIndex.predecessor())
    }
    //Convert dictionary to json
    class func getPostParamsURL(parameters: [String : AnyObject]) -> String {
        var getURL:String = ""
        for (key, value) in parameters {
            getURL = getURL + "\(key)" + "=" + "\(value)" + "&"
        }
        return getURL.substringToIndex(getURL.endIndex.predecessor())
    }
    class func generateBoundaryString() -> String{
        return "Boundary-\(NSUUID().UUIDString)"
    }

    //Convert dictionary to json
    class func encodeParameters(parameters: [String : AnyObject]) -> NSData {
        do {
            //Conver dictionary to data
            let data = try NSJSONSerialization.dataWithJSONObject(parameters, options: NSJSONWritingOptions.PrettyPrinted)
            return data
        } catch let error as NSError {
            //Return empty object
            print(error)
            return NSData()
        }
        
    }
    // Convert from NSData to json object
    class func nsdataToJSON(data: NSData) -> AnyObject? {
        do {
            return try NSJSONSerialization.JSONObjectWithData(data, options: .MutableContainers)
        } catch let myJSONError {
            print(myJSONError)
        }
        return nil
    }
    // Convert from JSON to nsdata
    func jsonToNSData(json: AnyObject) -> NSData?{
        do {
            return try NSJSONSerialization.dataWithJSONObject(json, options: NSJSONWritingOptions.PrettyPrinted)
        } catch let myJSONError {
            print(myJSONError)
        }
        return nil;
    }
}

//
//  AdmobAdsHandler.swift
//  PlainSwiftFramework
//
//  Created by Development on 12/18/15.
//  Copyright © 2015 maheshbabu.somineni. All rights reserved.
//

import UIKit
import GoogleMobileAds

class AdmobAdsHandler: NSObject, GADInterstitialDelegate {

    var delegate:BaseViewController!

    var adMobBannerView: GADBannerView!
    var admobInterstitial: GADInterstitial?
    var adShownFirstTime:Bool!
 
    override init() {
        
        self.adShownFirstTime = false
    }
    
    //MARK:Ads methods
    func loadAdMobBannerView(){
        
        //iAd
        self.adMobBannerView =  GADBannerView(adSize: kGADAdSizeBanner)
        if(UIDevice.currentDevice().userInterfaceIdiom == .Pad){
            self.adMobBannerView.frame = CGRectMake(delegate.view.bounds.size.width - (delegate.view.bounds.size.width)/1.4, delegate.view.bounds.size.height - 95, 320, 50)
        }else{
            self.adMobBannerView.frame = CGRectMake(0, delegate.view.bounds.size.height - 95, 320, 50)
        }
        self.adMobBannerView.adUnitID = GlobalVariables.admobBannerId
        self.adMobBannerView.rootViewController = delegate
        self.adMobBannerView.loadRequest(GADRequest())
        self.adMobBannerView.hidden = false
        delegate.view.addSubview(self.adMobBannerView)
        
    }
    func loadAdMobInterstitialAd() {
        
        self.admobInterstitial = GADInterstitial(adUnitID:GlobalVariables.admobInterstialId)
        self.admobInterstitial!.delegate = self
        let request = GADRequest()
        // Requests test ads on test devices.
        request.testDevices = ["2077ef9a63d2b398840261c8221a0c9b"]
        self.admobInterstitial!.loadRequest(request)
    }
    func showadMobInterstial(){
        
        if let interstial = self.admobInterstitial{
            
            if (interstial.isReady) {
                
                interstial.presentFromRootViewController(delegate)
                
                //Set the time 
                let dateFormatter = NSDateFormatter()
                dateFormatter.dateFormat = GlobalVariables.timeFormat
                let dateString = dateFormatter.stringFromDate(NSDate())
                
                GlobalSingleton.Static.adSwownTime = dateString
            }
        }
    }
    func interstitialDidReceiveAd(interstitial: GADInterstitial!) {

        if(self.adShownFirstTime == false){
            
            self.showadMobInterstial()
            self.adShownFirstTime = true
            
        }else{
            
             let calendarUnit:NSDateComponents = GlobalSettings.timeDifference(GlobalSingleton.getAdShownTime())
            if calendarUnit.minute > 1{
                
                self.showadMobInterstial()
                self.adShownFirstTime = true

            }else{
                
                return
            }
        }

    }
}

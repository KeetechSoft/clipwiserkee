//
//  PhotoStreamViewController.swift
//  RWDevCon
//
//  Created by Mic Pringle on 26/02/2015.
//  Copyright (c) 2015 Ray Wenderlich. All rights reserved.
//

import UIKit
import AVFoundation

class PhotoStreamViewController: BaseViewController {
    
    var productsArray = NSMutableArray()
    
    @IBOutlet weak var productsCollectionView: UICollectionView!
    override func preferredStatusBarStyle() -> UIStatusBarStyle {
        return UIStatusBarStyle.LightContent
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        if let patternImage = UIImage(named: "Pattern") {
            view.backgroundColor = UIColor(patternImage: patternImage)
        }
        // Set the PinterestLayout delegate
        if let layout = productsCollectionView?.collectionViewLayout as? PinterestLayout {
            layout.delegate = self
        }
        productsCollectionView!.backgroundColor = UIColor.clearColor()
        productsCollectionView!.contentInset = UIEdgeInsets(top: 23, left: 5, bottom: 10, right: 5)
        
        getProductsServiceCall()
    }
    // MARK: - Service Calls
    func getProductsServiceCall(){
        
        //Save to defaults
        super.showProgress()
        let params:NSDictionary = ["device_id": GlobalVariables.device_id, "ids":"1,2,3","page_no":"1","clipwiser_per_page":"6"]
        print(params)
        NetworkManager.getRequest(params as! [String : String],requestMethod: GlobalVariables.RequestAPIMethods.getProducts,  delegate: self)
        
    }
    
    // MARK: Service response handlers
    override func dataDelegate(responseData: AnyObject, requestMethod: GlobalVariables.RequestAPIMethods) {
        super.dataDelegate(responseData, requestMethod: requestMethod)
        let jsonCategoriesArray = responseData["all_categories"] as! [AnyObject]
        
        for dictionary in jsonCategoriesArray {
            let product:Product = Product(dictionary: dictionary as! [String : AnyObject])
            productsArray.addObject(product)
        }
        
        self.productsCollectionView.reloadData()
    }
    func heightForComment(font: UIFont, width: CGFloat, comment:String) -> CGFloat {
        let rect = NSString(string: comment).boundingRectWithSize(CGSize(width: width, height: CGFloat(MAXFLOAT)), options: .UsesLineFragmentOrigin, attributes: [NSFontAttributeName: font], context: nil)
        return ceil(rect.height)
    }
}

extension PhotoStreamViewController:UICollectionViewDataSource,UICollectionViewDelegate {
    
    func collectionView(collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return productsArray.count
    }
    
    func collectionView(collectionView: UICollectionView, cellForItemAtIndexPath indexPath: NSIndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCellWithReuseIdentifier("AnnotatedPhotoCell", forIndexPath: indexPath) as! AnnotatedPhotoCell
        
        let object: Product = productsArray.objectAtIndex(indexPath.row) as! Product
        cell.imageView.sd_setImageWithURL(NSURL(string: object.image_url!))
        cell.commentLabel.text = object.descr
        cell.captionLabel.text = object.product_name
        
        return cell
    }
    
}

extension PhotoStreamViewController : PinterestLayoutDelegate {
    // 1. Returns the photo height
    func collectionView(collectionView:UICollectionView, heightForPhotoAtIndexPath indexPath:NSIndexPath , withWidth width:CGFloat) -> CGFloat {

        //let cell:AnnotatedPhotoCell = productsCollectionView.cellForItemAtIndexPath(indexPath) as! AnnotatedPhotoCell
        
        let boundingRect =  CGRect(x: 0, y: 0, width: width, height: CGFloat(MAXFLOAT))
        
        //let image = cell.imageView.image?.decompressedImage
        let rect  = AVMakeRectWithAspectRatioInsideRect(UIImage(named: "04.png")!.size, boundingRect)
        return rect.size.height
    }
    
    // 2. Returns the annotation size based on the text
    func collectionView(collectionView: UICollectionView, heightForAnnotationAtIndexPath indexPath: NSIndexPath, withWidth width: CGFloat) -> CGFloat {
        let annotationPadding = CGFloat(4)
        let annotationHeaderHeight = CGFloat(17)
        
        let object: Product = productsArray.objectAtIndex(indexPath.row) as! Product
        let font = UIFont(name: "AvenirNext-Regular", size: 10)!
        let commentHeight = heightForComment(font, width: width, comment: object.descr!)
        let height = annotationPadding + annotationHeaderHeight + commentHeight + annotationPadding
        return height
    }
}


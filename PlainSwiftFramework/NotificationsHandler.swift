//
//  NotificationsHandler.swift
//  PlainSwiftFramework
//
//  Created by apple on 12/13/15.
//  Copyright © 2015 maheshbabu.somineni. All rights reserved.
//

import UIKit

class NotificationsHandler: NSObject {

    class func addNotification(dictionary:AnyObject) {
     
        let localNotification = UILocalNotification() // Creating an instance of the notification.
        localNotification.alertTitle = GlobalVariables.appName
        
        if let body = dictionary.valueForKey("note"){
            localNotification.alertBody = body as? String
        }
        localNotification.alertAction = "ShowDetails"

        if let remainderDate:NSDate = dictionary.valueForKey("remainderDate") as? NSDate{
            
            localNotification.fireDate = remainderDate
        }
        
        localNotification.soundName = UILocalNotificationDefaultSoundName // Use the default notification tone/ specify a file in the application bundle
        localNotification.category = "Personal" // Category to use the specified actions
    
        //Setting primary key
        if let objectId = dictionary.valueForKey("objectId") as? String{
            
            let infoDict :  Dictionary<String,String!> = ["objectId" :objectId]
            localNotification.userInfo = infoDict;
        }
        UIApplication.sharedApplication().scheduleLocalNotification(localNotification) // Scheduling the notification.
    }
    class func updateNotification(dictionary:AnyObject) {
    
        if let objectId = dictionary.valueForKey("objectId") as? String{
            let app:UIApplication = UIApplication.sharedApplication()
            
            var isNotificationAvailableBool:Bool = false
            for event in app.scheduledLocalNotifications! {
                
                let notification = event
                var infoDict :  Dictionary = notification.userInfo as! Dictionary<String,String!>
                let notifcationObjectId : String = infoDict["objectId"]!
                if notifcationObjectId == objectId {
                    
                    isNotificationAvailableBool = true
                    //Cancel and add as new notification
                    app.cancelLocalNotification(notification)
                    NotificationsHandler.addNotification(dictionary)
                
                }
            }
            if(!isNotificationAvailableBool){
                NotificationsHandler.addNotification(dictionary)
            }
        }
    }
    class func cancelNotification(objectId:String) {
    
        let app:UIApplication = UIApplication.sharedApplication()
        for event in app.scheduledLocalNotifications! {
            
            let notification = event
            var infoDict :  Dictionary = notification.userInfo as! Dictionary<String,String!>
           
            let notifcationObjectId : String = infoDict["objectId"]!
            if notifcationObjectId == objectId {
                app.cancelLocalNotification(notification)
            }
        }
    }
    class func cancelAllNotifications() {
        
        UIApplication.sharedApplication().cancelAllLocalNotifications() // Scheduling the notification.

    }

}

//
//  AageCalcViewController.swift
//  PlainSwiftFramework
//
//  Created by mahesh on 12/04/16.
//  Copyright © 2016 maheshbabu.somineni. All rights reserved.
//

import UIKit

enum AgeCalcTableViewCellEnum : Int {
    case DateOfBirthDateCell = 0
    case TargetDateCell = 1
    case CalculateButtonsCell = 2
    case CalcDetailsCell = 3
    case ShareButtonsCell = 4
}
enum PickerViewTag : Int {
    case DateOfBirthTag = 0
    case TargetDateTag = 1
}
struct Calculator {
    
    var dateOfBirth:String
    var targetDate:String
    var nextBirthdayDate:String
    var age:String
    var years: String
    var months: String
    var weeks:String
    var days:String
    var hours:String
    var minutes:String
    var seconds:String
}
class AageCalcViewController: BaseViewController {
    
    @IBOutlet weak var tableView: UITableView!
    var datePickerView: UIDatePicker!
    var calculatorObject:Calculator?
    var dateOfBirthTextFiled:UITextField?
    var targetDateTextFiled:UITextField?
    var isEdit:Bool = false
    var birdayObject:BirthdaysModel?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // Do any additional setup after loading the view.
        self.setUIProperties()
    }
    func setUIProperties(){
        
        if self.isEdit{
            self.title = self.birdayObject!.name
            self.calculatorObject = Calculator(dateOfBirth:(self.birdayObject?.dateOfBirth)!,targetDate:(self.birdayObject?.targetDate)! , nextBirthdayDate :(self.birdayObject?.nextBirthdayDate)!,age: (self.birdayObject?.age)!, years: (self.birdayObject?.years)!, months: (self.birdayObject?.months)!, weeks: (self.birdayObject?.weeks)!, days: (self.birdayObject?.days)!, hours: (self.birdayObject?.hours)!, minutes: (self.birdayObject?.minutes)!, seconds: (self.birdayObject?.seconds)!)
        }else{
            super.showMenuButton()
            self.calculatorObject = Calculator(dateOfBirth:"",targetDate:GlobalSettings.getDateString(NSDate()) , nextBirthdayDate :"",age: "", years: "", months: "", weeks: "", days: "", hours: "", minutes: "", seconds: "")
        }
        
        //Set background image
        self.view.addBackground("main_background")
        
        //Table view background
        self.tableView.backgroundColor = UIColor.clearColor()
        
        // screen width and height:
        self.tableView.addBackgroundBorder()
        
    }
    func refreshView(){
        self.tableView.reloadData()
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    
    /*
    // MARK: - Navigation
    
    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
    // Get the new view controller using segue.destinationViewController.
    // Pass the selected object to the new view controller.
    }
    */
    func addDropdownButton(textFiled:UITextField){
        
        let rightview = UIView()
        let image = UIImage(named: "dropdown_button.png") as UIImage?
        rightview.frame = CGRectMake(0, 0, 30, 30)
        let dropdownButton = UIButton(frame :CGRect(x: 0, y: 0, width: 30  , height: 30))
        dropdownButton.setBackgroundImage(image, forState: .Normal)
        rightview.addSubview(dropdownButton)
        textFiled.rightView = rightview;
        textFiled.rightViewMode = UITextFieldViewMode.Always
        dropdownButton.addTarget(self, action: #selector(AageCalcViewController.dropdownButtonClick(_:)), forControlEvents: .TouchUpInside)
        
    }
    func calculateAge(){
        
        let dateFormatter = NSDateFormatter()
        dateFormatter.dateFormat = GlobalVariables.timeFormat
        
        let startDate:NSDate = (self.calculatorObject?.targetDate.dateFromString())!
        let endDate:NSDate = (self.calculatorObject?.dateOfBirth.dateFromString())!
        
        self.calculatorObject = startDate.offsetFrom(endDate, calculator: self.calculatorObject!)
        print("Days = \(self.calculatorObject)")
        self.refreshView()
    }
    
    //MARK: Button clicks
    @IBAction func settingsButtonClick(sender:AnyObject){
        super.showSettingsActionSheet()
    }
    func saveButtonClick(sender:AnyObject){
        
        //1. Create the alert controller.
        let alert = UIAlertController(title:GlobalVariables.appName, message: "Please enter name", preferredStyle: .Alert)
        
        //2. Add the text field. You can configure it however you need.
        alert.addTextFieldWithConfigurationHandler({ (textField) -> Void in
        })
        
        //3. Grab the value from the text field, and print it when the user clicks OK.
        alert.addAction(UIAlertAction(title: "OK", style: .Default, handler: { (action) -> Void in
            let textField = alert.textFields![0] as UITextField
            
            if textField.text?.lengthOfBytesUsingEncoding(NSUTF8StringEncoding) == 0{
                
                super.showAlertView(GlobalVariables.appName, alertMsg: "Please enter name.")
                
            }else{
                let note:NSDictionary = ["name": textField.text!, "dateOfBirth":(self.calculatorObject?.dateOfBirth)! ,"targetDate":(self.calculatorObject?.targetDate)! , "nextBirthdayDate":(self.calculatorObject?.nextBirthdayDate)! ,"age":(self.calculatorObject?.age)!,"years":(self.calculatorObject?.years)!,"months":(self.calculatorObject?.months)!,"weeks":(self.calculatorObject?.weeks)!,"days":(self.calculatorObject?.days)!,"hours":(self.calculatorObject?.hours)!,"minutes":(self.calculatorObject?.minutes)!,"seconds":(self.calculatorObject?.seconds)!,"savedDate":GlobalSettings.getDateString(NSDate()),"checked":NSNumber(bool: false)]
                BirthdaysModel.insertObject(note as AnyObject, context: super.appDelegate.managedObjectContext)
                
                //super.appDelegate.changeMenu(DetailsViewControllerEnum.BirthdaysListViewController.rawValue)
            }
            
        }))
        alert.addAction(UIAlertAction(title: "Cancel", style: .Cancel, handler: { (action) -> Void in
            
        }))
        // 4. Present the alert.
        self.presentViewController(alert, animated: true, completion: nil)
    }
    func dropdownButtonClick(sender:AnyObject){
        
        let button:UIButton = sender as! UIButton
        let buttonPosition = button.convertPoint(CGPointZero, toView: self.tableView)
        let indexPath = self.tableView.indexPathForRowAtPoint(buttonPosition)
        
        if indexPath?.row == AgeCalcTableViewCellEnum.DateOfBirthDateCell.rawValue {
            self.showPickerView(PickerViewTag.DateOfBirthTag.rawValue)
        }else{
            self.showPickerView(PickerViewTag.TargetDateTag.rawValue)
        }
        self.datePickerView.date = (self.calculatorObject?.targetDate.dateFromString())!
    }
    func calculateButtonClick(sender:AnyObject){
        
        if self.dateOfBirthTextFiled?.text?.lengthOfBytesUsingEncoding(NSUTF8StringEncoding) == 0{
            super.showAlertView(GlobalVariables.appName, alertMsg: "Please select date of birth.")
        }else{
            self.calculateAge()
        }
    }
    func shareButtonClick(sender:AnyObject){
        
        let textToShare = "Birthday: \(self.calculatorObject!.dateOfBirth) \n Target Date: \(self.calculatorObject!.targetDate) \n Age: \(self.calculatorObject!.age) \n Next Birthday in \(self.calculatorObject!.nextBirthdayDate)"
        
        if let myWebsite = NSURL(string: "http://sptit.com/") {
            let objectsToShare = [textToShare, myWebsite]
            let activityVC = UIActivityViewController(activityItems: objectsToShare, applicationActivities: nil)
            
            activityVC.popoverPresentationController?.sourceView = self.view
            activityVC.popoverPresentationController?.sourceRect = CGRectMake(self.view.bounds.width / 2.0, self.view.bounds.height / 2.0, 1.0, 1.0) // this is the center of the screen currently but it can be any point in the view
            activityVC.popoverPresentationController?.permittedArrowDirections = UIPopoverArrowDirection.Down
            
            // 5
            self.presentViewController(activityVC, animated: true, completion: nil)
        }
    }
    
    //MARK: Picker methods
    func showPickerView(pickerViewTag:Int){
        
        //Creating pickerview object
        if(UIDevice.currentDevice().userInterfaceIdiom == .Pad){
            self.datePickerView = UIDatePicker(frame: CGRectMake(-225, 0, self.view.frame.size.width, 200))
        }else{
            self.datePickerView = UIDatePicker(frame: CGRectMake(-10, 0, self.view.frame.size.width, 200))
        }
        self.datePickerView.datePickerMode = UIDatePickerMode.Date
        
        //Creating view to add datepicker
        let viewPicker: UIView = UIView(frame: CGRectMake(0, 0, self.view.frame.size.width, 200))
        viewPicker.backgroundColor = UIColor.clearColor()
        
        //Initializing date picker
        self.datePickerView.tag = pickerViewTag
        viewPicker.addSubview(self.datePickerView)
        
        let alertController = UIAlertController(title: GlobalVariables.appName, message:"\n\n\n\n\n\n\n\n\n", preferredStyle: .ActionSheet)
        alertController.view.addSubview(viewPicker)
        
        let cancelAction = UIAlertAction(title: "Cancel", style: .Cancel) { (action) in
            
            // ...
        }
        alertController.addAction(cancelAction)
        
        let OKAction = UIAlertAction(title: "OK", style: .Default) { (action) in
            
            if self.datePickerView.tag == AgeCalcTableViewCellEnum.DateOfBirthDateCell.rawValue {
                self.calculatorObject?.dateOfBirth = GlobalSettings.getDateString(self.datePickerView.date)
                
            }else{
                self.calculatorObject?.targetDate = GlobalSettings.getDateString(self.datePickerView.date)
            }
            //Update note
            self.refreshView()
        }
        alertController.addAction(OKAction)
        alertController.popoverPresentationController?.sourceView = self.view
        alertController.popoverPresentationController?.sourceRect = CGRectMake(self.view.bounds.width / 2.0, self.view.bounds.height / 2.0, 1.0, 1.0) // this is the center of the screen currently but it can be any point in the view
        alertController.popoverPresentationController?.permittedArrowDirections = UIPopoverArrowDirection.Down
        self.presentViewController(alertController, animated: true, completion: nil)
    }
    
}
extension AageCalcViewController : UITableViewDelegate, UITableViewDataSource{
    
    func numberOfSectionsInTableView(tableView: UITableView) -> Int {
        
        return 1
    }
    func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        // Return the number of rows in the section.
        return 5
    }
    func tableView(tableView: UITableView, heightForRowAtIndexPath indexPath: NSIndexPath) -> CGFloat{
        
        var cellHeight:CGFloat = 0
        
       
        if UIDevice.currentDevice().userInterfaceIdiom == .Pad{
            
            switch indexPath.row {
            case AgeCalcTableViewCellEnum.DateOfBirthDateCell.rawValue:
                cellHeight = 70
                break
            case AgeCalcTableViewCellEnum.TargetDateCell.rawValue:
                cellHeight = 70
                break
            case AgeCalcTableViewCellEnum.CalculateButtonsCell.rawValue:
                cellHeight = 44
                break
            case AgeCalcTableViewCellEnum.CalcDetailsCell.rawValue:
                cellHeight = 400
                break
            default:
                cellHeight = 44
                break
            }
            return cellHeight
            
        }else{
            
            let result : CGSize = UIScreen.mainScreen().bounds.size
            
            if(result.height == 480){
                
                switch indexPath.row {
                case AgeCalcTableViewCellEnum.DateOfBirthDateCell.rawValue:
                    cellHeight = 70
                    break
                case AgeCalcTableViewCellEnum.TargetDateCell.rawValue:
                    cellHeight = 70
                    break
                case AgeCalcTableViewCellEnum.CalculateButtonsCell.rawValue:
                    cellHeight = 50
                    break
                case AgeCalcTableViewCellEnum.CalcDetailsCell.rawValue:
                    cellHeight = 170
                    break
                default:
                    cellHeight = 44
                    break
                }
                return cellHeight
                
            }else if(result.height == 568){
                
                switch indexPath.row {
                case AgeCalcTableViewCellEnum.DateOfBirthDateCell.rawValue:
                    cellHeight = 70
                    break
                case AgeCalcTableViewCellEnum.TargetDateCell.rawValue:
                    cellHeight = 70
                    break
                case AgeCalcTableViewCellEnum.CalculateButtonsCell.rawValue:
                    cellHeight = 44
                    break
                case AgeCalcTableViewCellEnum.CalcDetailsCell.rawValue:
                    cellHeight = 230
                    break
                default:
                    cellHeight = 44
                    break
                }
                return cellHeight
                
            }else{
                
                
                switch indexPath.row {
                case AgeCalcTableViewCellEnum.DateOfBirthDateCell.rawValue:
                    cellHeight = 70
                    break
                case AgeCalcTableViewCellEnum.TargetDateCell.rawValue:
                    cellHeight = 70
                    break
                case AgeCalcTableViewCellEnum.CalculateButtonsCell.rawValue:
                    cellHeight = 44
                    break
                case AgeCalcTableViewCellEnum.CalcDetailsCell.rawValue:
                    cellHeight = 230
                    break
                default:
                    cellHeight = 44
                    break
                }
                return cellHeight
                
                
            }
            
        }
    }
    func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        
        
        let cell:UITableViewCell!
        
        switch indexPath.row {
            
        case AgeCalcTableViewCellEnum.DateOfBirthDateCell.rawValue:
            cell = tableView.dequeueReusableCellWithIdentifier("DateOfBirthDateCell", forIndexPath: indexPath)
            
            //Working with cell controls
            let textFiled = cell.contentView.viewWithTag(1001) as! UITextField
            let button = cell.contentView.viewWithTag(1002) as! UIButton
            button.addTarget(self, action: #selector(AageCalcViewController.dropdownButtonClick(_:)), forControlEvents: .TouchUpInside)

            self.dateOfBirthTextFiled = textFiled
            textFiled.text = self.calculatorObject?.dateOfBirth
            
            break
        case AgeCalcTableViewCellEnum.TargetDateCell.rawValue:
            cell = tableView.dequeueReusableCellWithIdentifier("TargetDateCell", forIndexPath: indexPath)
            
            //Working with cell controls
            let textFiled = cell.contentView.viewWithTag(1001) as! UITextField
            let button = cell.contentView.viewWithTag(1002) as! UIButton
            button.addTarget(self, action: #selector(AageCalcViewController.dropdownButtonClick(_:)), forControlEvents: .TouchUpInside)
        
            self.targetDateTextFiled = textFiled
            textFiled.text = self.calculatorObject?.targetDate
            
            break
        case AgeCalcTableViewCellEnum.CalculateButtonsCell.rawValue:
            cell = tableView.dequeueReusableCellWithIdentifier("CalculateButtonsCell", forIndexPath: indexPath)
            
            //Working with cell controls
            let button = cell.contentView.viewWithTag(1001) as! UIButton
            button.layer.cornerRadius = 4
            button.addTarget(self, action:#selector(AageCalcViewController.calculateButtonClick(_:)), forControlEvents: UIControlEvents.TouchUpInside)
            
            break
        case AgeCalcTableViewCellEnum.CalcDetailsCell.rawValue:
            cell = tableView.dequeueReusableCellWithIdentifier("CalcDetailsCell", forIndexPath: indexPath)
            
            //Working with cell controls
            let ageLable = cell.contentView.viewWithTag(1001) as! UILabel
            let yearsLable = cell.contentView.viewWithTag(1002) as! UILabel
            let monthsLable = cell.contentView.viewWithTag(1003) as! UILabel
            let weeksLable = cell.contentView.viewWithTag(1004) as! UILabel
            let daysLable = cell.contentView.viewWithTag(1005) as! UILabel
            let hoursLable = cell.contentView.viewWithTag(1006) as! UILabel
            let minituesLable = cell.contentView.viewWithTag(1007) as! UILabel
            let secondsLable = cell.contentView.viewWithTag(1008) as! UILabel
            
            //Set values
            ageLable.text = "Age: \(self.calculatorObject!.age)"
            yearsLable.text = "Years: \(self.calculatorObject!.years)"
            monthsLable.text = "Months: \(self.calculatorObject!.months)"
            weeksLable.text = "Weeks: \(self.calculatorObject!.weeks)"
            daysLable.text = "Days: \(self.calculatorObject!.days)"
            hoursLable.text = "Hours: \(self.calculatorObject!.hours)"
            minituesLable.text = "Minutes: \(self.calculatorObject!.minutes)"
            secondsLable.text = "Seconds: \(self.calculatorObject!.seconds)"
            print(self.calculatorObject?.nextBirthdayDate)
            
            break
        default:
            cell = tableView.dequeueReusableCellWithIdentifier("ShareButtonsCell", forIndexPath: indexPath)
            
            //Working with cell controls
            let button = cell.contentView.viewWithTag(1001) as! UIButton
            button.addTarget(self, action:#selector(AageCalcViewController.shareButtonClick(_:)), forControlEvents: UIControlEvents.TouchUpInside)
            
            let saveButton = cell.contentView.viewWithTag(1002) as! UIButton
            saveButton.addTarget(self, action:#selector(AageCalcViewController.saveButtonClick(_:)), forControlEvents: UIControlEvents.TouchUpInside)
            break
        }
        
        cell.clearBackgroundColors()
        return cell
    }
    
}


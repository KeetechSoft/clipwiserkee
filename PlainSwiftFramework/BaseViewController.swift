//
//  BaseViewController.swift
//  SwiftCoreDataSimpleDemo
//
//  Created by maheshbabu.somineni on 12/10/15.
//  Copyright © 2015 CHENHAO. All rights reserved.
//

import UIKit
import MBProgressHUD

protocol MenuButtonDelegate {
    
    func toggleLeftPanel()
    func collapseSidePanels()
}

class BaseViewController: UIViewController, CMNetworkDelegate {
    
    var menuButtonDlegate: MenuButtonDelegate?
    var progressHUD = MBProgressHUD()
    var inappsHandler:InAppPurchaseHandler = InAppPurchaseHandler()
    var admobsHandler:AdmobAdsHandler = AdmobAdsHandler()
    let appDelegate:AppDelegate! = UIApplication.sharedApplication().delegate as! AppDelegate

    override func viewDidLoad() {
        super.viewDidLoad()

        GlobalSettings.increaseAppIterationNumber()
        
        //Get products info
        self.inappsHandler.delegate = self
        self.inappsHandler.getProductInfo()
        self.admobsHandler.delegate = self
    }
    override func viewWillAppear(animated: Bool) {
        super.viewWillAppear(animated)
        
        //Adding ad views
        /*if(!GlobalSettings.isPurchased()){
            
            self.admobsHandler.loadAdMobBannerView()
            self.admobsHandler.loadAdMobInterstitialAd()
        }*/
    }
    func setViewBackGround(view:UIView){
        
        // Do any additional setup after loading the view.
        let backImage = UIImage(named: "main_background")
        let resizablebackImage = backImage?.resizableImageWithCapInsets(UIEdgeInsets(top:10,left:0,bottom:10,right:0))
        view.backgroundColor = UIColor(patternImage:resizablebackImage!)

    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func showProgress(){
       
        //Show the progress bard
        progressHUD = MBProgressHUD.showHUDAddedTo(self.view, animated: true)
        progressHUD.labelText = "Loading..."
        
    }
    func showAlertView(alertTitle: String, alertMsg: String){
        
        let alertController = UIAlertController(title: alertTitle, message: alertMsg, preferredStyle: .Alert)
        
        let defaultAction = UIAlertAction(title: "OK", style: .Default, handler: nil)
        alertController.addAction(defaultAction)
        
        presentViewController(alertController, animated: true, completion: nil)
        
        let delay = 2.0 * Double(NSEC_PER_SEC)
        let time = dispatch_time(DISPATCH_TIME_NOW, Int64(delay))
        dispatch_after(time, dispatch_get_main_queue(), {
            
            //Do some other stuff
            alertController.dismissViewControllerAnimated(true, completion: {
                
            })
        })
    }
    func hideProgress(){
        
        //Show the progress bard
        progressHUD.hide(true)
        
    }
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */
    //MARK:Menu button delegate methods
    func showMenuButton(){
     
        let menuButton : UIBarButtonItem = UIBarButtonItem()
        menuButton.style = UIBarButtonItemStyle.Plain
        menuButton.target = self
        menuButton.action = #selector(BaseViewController.menuButtonClick)
        menuButton.image = UIImage(named: "menu_button")
        self.navigationItem.leftBarButtonItem = menuButton

    }
    func menuButtonClick() {
        
        super.view.endEditing(true)
        self.menuButtonDlegate!.toggleLeftPanel()
        
    }
    //MARK:Service delegate methods
    func dataDelegate(reponseData:AnyObject, requestMethod:GlobalVariables.RequestAPIMethods){
        
        progressHUD.hide(true)
        print("\(requestMethod) = \(reponseData)")
    }
    func networkError(errorMessage:String){
        
        progressHUD.hide(true)
        GlobalSettings.showAlertView(GlobalVariables.appName, alertMsg: errorMessage, target: self)
    }
    
    func showSettingsActionSheet(){
        
        let optionMenu = UIAlertController(title: nil, message: "Choose Option", preferredStyle: .ActionSheet)
        

        _ = UIAlertAction(title: "Remove Ads", style: .Default, handler: {
            (alert: UIAlertAction!) -> Void in
            
            self.buyProductOrRemoveAds()
        })
        
        let ratingAction = UIAlertAction(title: "Rate the app", style: .Default, handler: {
            (alert: UIAlertAction!) -> Void in
            
            UIApplication.sharedApplication().openURL(NSURL(string: GlobalVariables.appReviewURL )!)
        })
        _ = UIAlertAction(title: "Restore", style: .Default, handler: {
            (alert: UIAlertAction!) -> Void in
            
            self.restoreProducts()
        })

        let moreApps = UIAlertAction(title: "More Apps", style: .Default, handler: {
            (alert: UIAlertAction!) -> Void in
            
            UIApplication.sharedApplication().openURL(NSURL(string: GlobalVariables.moreAppsURL)!)
            
        })
        
        let cancelAction = UIAlertAction(title: "Cancel", style: .Cancel, handler: {
            (alert: UIAlertAction!) -> Void in
            print("Cancelled")
        })
        
        //Add remove ads button if the app is not purchased
        /*if(!GlobalSettings.isPurchased()){
            
            optionMenu.addAction(removeAdsAction)
            optionMenu.addAction(restoreAction)

            
        }*/
        optionMenu.addAction(moreApps)
        optionMenu.addAction(ratingAction)
        optionMenu.addAction(cancelAction)
        
        optionMenu.popoverPresentationController?.sourceView = self.view
        optionMenu.popoverPresentationController?.sourceRect = CGRectMake(self.view.bounds.width / 2.0, self.view.bounds.height / 2.0, 1.0, 1.0) // this is the center of the screen currently but it can be any point in the view
        optionMenu.popoverPresentationController?.permittedArrowDirections = UIPopoverArrowDirection.Down
        
        // 5
        self.presentViewController(optionMenu, animated: true, completion: nil)
    }
    func buyProductOrRemoveAds(){
        
        self.inappsHandler.buyProduct()
    }
    func restoreProducts(){
        
        self.inappsHandler.restoreProducts()
    }
    func showInterestialAd(){
        
        self.admobsHandler.showadMobInterstial()
    }
    
   
}

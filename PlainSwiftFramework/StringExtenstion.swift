//
//  StringExtenstion.swift
//  PlainSwiftFramework
//
//  Created by mahesh on 13/04/16.
//  Copyright © 2016 maheshbabu.somineni. All rights reserved.
//

import Foundation
extension String {
    
    func dateFromString() -> NSDate{
        
        let dateFormatter = NSDateFormatter()
        dateFormatter.dateFormat = GlobalVariables.timeFormat
        return dateFormatter.dateFromString(self)!
        
    }
}

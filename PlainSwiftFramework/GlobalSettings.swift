//
//  GlobalSettings.swift
//  GameFramework
//
//  Created by apple on 1/16/15.
//  Copyright (c) 2015 apple. All rights reserved.
//

import Foundation

class GlobalSettings {
    
    class func addUserDefaults(){
        GlobalVariables.globalUserDefaults.setInteger(0, forKey: GlobalVariables.user_defaults_app_iteration_number_key)
        GlobalVariables.globalUserDefaults.setBool(GlobalVariables.isPurchased, forKey: GlobalVariables.user_defaults_isPurchased_key)
        GlobalVariables.globalUserDefaults.setValue("\(true)", forKey: GlobalVariables.show_introduction_view_key)
        GlobalVariables.globalUserDefaults.setValue("", forKey: GlobalVariables.access_token_key)
        GlobalVariables.globalUserDefaults.setValue("", forKey: GlobalVariables.user_defaults_deviceid_key)

        NSUserDefaults.standardUserDefaults().synchronize()
    }
    
    class func updateUserDefaultValue(key:String, value:String) {
        print(value)
    
        switch(key) {
        case GlobalVariables.user_defaults_isPurchased_key :
            GlobalVariables.globalUserDefaults.setValue(value, forKey: GlobalVariables.user_defaults_isPurchased_key)
            break
        case GlobalVariables.show_introduction_view_key :
            GlobalVariables.globalUserDefaults.setValue(value, forKey: GlobalVariables.show_introduction_view_key)
            break
        case GlobalVariables.access_token_key :
            GlobalVariables.globalUserDefaults.setValue(value, forKey: GlobalVariables.access_token_key)
            break
        case GlobalVariables.user_defaults_deviceid_key :
            GlobalVariables.globalUserDefaults.setValue(value, forKey: GlobalVariables.user_defaults_deviceid_key)
            break
        default:
            print("Default case")
            break
        }
        GlobalVariables.globalUserDefaults.synchronize()
    }
    
    class func getUserDefaultValue(key:String) -> String {
        
        switch(key) {
        case GlobalVariables.user_defaults_isPurchased_key :
            return GlobalVariables.globalUserDefaults.stringForKey(GlobalVariables.user_defaults_isPurchased_key)!
        case GlobalVariables.show_introduction_view_key :
            return GlobalVariables.globalUserDefaults.stringForKey(GlobalVariables.show_introduction_view_key)!
        case GlobalVariables.access_token_key :
            return GlobalVariables.globalUserDefaults.stringForKey(GlobalVariables.access_token_key)!
        case GlobalVariables.user_defaults_deviceid_key :
            return uuid = GlobalVariables.globalUserDefaults.valueForKey(GlobalVariables.user_defaults_deviceid_key)
        default:
            return ""
        }
    }
    
    class func increaseAppIterationNumber(){
        var iterationNumber = GlobalVariables.globalUserDefaults.integerForKey(GlobalVariables.user_defaults_app_iteration_number_key)
        iterationNumber = iterationNumber+1;
        GlobalVariables.globalUserDefaults.setInteger(iterationNumber, forKey: GlobalVariables.user_defaults_app_iteration_number_key)
        NSUserDefaults.standardUserDefaults().synchronize()
    }
    
    class func getUUID() -> String    {
        
        var uuid = GlobalVariables.globalUserDefaults.valueForKey(GlobalVariables.user_defaults_deviceid_key)
        
        if uuid == ""{
            let theUUID = CFUUIDCreate(nil)  //CFUUIDCreate(NULL);
            let stringUUID = CFUUIDCreateString(nil, theUUID);
            return stringUUID as String
        }else{
            return uuid
        }
    }
    class func updateUUID(value: Bool){
        GlobalVariables.globalUserDefaults.setBool(value, forKey: GlobalVariables.user_defaults_isPurchased_key)
        NSUserDefaults.standardUserDefaults().synchronize()
    }

    //Logic for getting time difference between two dates.
    class func timeDifference(timeString:String) -> NSDateComponents{
        
        let dateFormatter = NSDateFormatter()
        dateFormatter.dateFormat = GlobalVariables.timeFormat
        
        let calendar: NSCalendar = NSCalendar.currentCalendar()
        let startDate:NSDate = NSDate()
        let endDate:NSDate = dateFormatter.dateFromString(timeString)!
        
        // Replace the hour (time) of both dates with 00:00
        let date1 = calendar.startOfDayForDate(startDate)
        let date2 = calendar.startOfDayForDate(endDate)
        
        let components = calendar.components( [NSCalendarUnit.Year,NSCalendarUnit.Day, NSCalendarUnit.Minute,NSCalendarUnit.Second,NSCalendarUnit.Nanosecond] , fromDate: date1, toDate: date2, options: [])
        
        return components
    }
    class func setIsPurchased(value: Bool){
        GlobalVariables.globalUserDefaults.setBool(value, forKey: GlobalVariables.user_defaults_isPurchased_key)
        NSUserDefaults.standardUserDefaults().synchronize()
    }

    class func getDateString(date:NSDate) -> String {
        
        let dateFormatter = NSDateFormatter()
        dateFormatter.dateFormat = GlobalVariables.timeFormat
        
        //timezone is added by mounika
        //need to be verified
        let timeInString = dateFormatter.stringFromDate(date)
        return timeInString
    }
    class func setButtonProperties(button:UIButton){
        button.layer.cornerRadius = 8
        button.layer.borderWidth = 4
        button.layer.borderColor = UIColor.whiteColor().CGColor
        button.backgroundColor = UIColor.clearColor()
    }
    class func setScoreboardButtonProperties(button:UIButton){
        button.layer.cornerRadius = 4
        button.layer.borderWidth = 1
        button.layer.borderColor = UIColor.blackColor().CGColor
        button.backgroundColor = UIColor.whiteColor()
    }
    class func setTextfiledProperties(textFiled:UITextField){
        textFiled.layer.borderColor = UIColor.clearColor().CGColor
    }
    enum TableViewCellEnum : Int {
        
        case kTimerCell = 0
        case kTriesCell = 1
        case kGoalsCell = 2
        case kPenaltiesCell = 3
        case kConversionsCell = 4
        case kTotalCell = 5
        case kMenuCell = 6
    }

    // MARK: Colours functions
    class func RGBA(r r:CGFloat, g:CGFloat, b:CGFloat, a:CGFloat)->UIColor{
        
        let color: UIColor = UIColor( red: CGFloat(r/255.0), green: CGFloat(g/255.0), blue: CGFloat(b/255.0), alpha: CGFloat(a) )
        return color
    }
    
    class func isImageAvailable(photo_url:NSString) -> Bool {
        let fileUrl = NSURL(string: photo_url as String)
        let fileManager = NSFileManager.defaultManager()
        let imagePath = fileUrl?.lastPathComponent
        let path = NSTemporaryDirectory() + imagePath!
    
        if (fileManager.fileExistsAtPath(path)){
            return true
        }else{
            return false
        }
    }
  
    class func setNavigationBarProperties() {
        
        //Navigation bar settings
        let navbarFont = UIFont(name: "Ubuntu", size: 17) ?? UIFont.systemFontOfSize(17)
        //let barbuttonFont = UIFont(name: "Ubuntu-Light", size: 15) ?? UIFont.systemFontOfSize(15)
        
        UINavigationBar.appearance().titleTextAttributes = [NSFontAttributeName: navbarFont, NSForegroundColorAttributeName:UIColor.whiteColor()]
        
        //UINavigationBar.appearance().barTintColor = UIColor(rgba: GlobalVariables.appColor)
        UINavigationBar.appearance().tintColor = UIColor.whiteColor()

    }
    
    class func showAlertView(alertTitle: String, alertMsg: String, target:AnyObject){
        
        let alertController = UIAlertController(title: alertTitle, message: alertMsg, preferredStyle: .Alert)
        let defaultAction = UIAlertAction(title: "OK", style: .Default, handler: nil)
        alertController.addAction(defaultAction)
        
        target.presentViewController(alertController, animated: true, completion: nil)
    }
    
}
//
//  Product.swift
//  PlainSwiftFramework
//
//  Created by mahesh on 08/10/16.
//  Copyright © 2016 maheshbabu.somineni. All rights reserved.
//

import UIKit
class Product: NSObject {
    
    let category_id: String?
    let name: String?
    let display_name: String?
    let created_at: String?
    let updated_at: String?
    let product_id: String?
    let product_name: String?
    let source: String?
    let source_url: String?
    let price: String?
    let descr: String?
    let rating: String?
    let image_url: String?
    let added_time: String?
    let updated_time: String?
    let user_clipped: Int?
    let clip_count: Int?
    let board_name: String?
    let first_name: String?
    let last_name: String?

    init(dictionary:[String:AnyObject]) {
        
        self.name = dictionary["name"] as? String
        self.display_name = dictionary["display_name"] as? String
        self.created_at = dictionary["created_at"] as? String
        self.updated_at = dictionary["updated_at"] as? String
        self.product_id = dictionary["product_id"] as? String
        self.product_name = dictionary["product_name"] as? String
        self.source = dictionary["source"] as? String
        self.source_url = dictionary["source_url"] as? String
        self.price = dictionary["price"] as? String
        self.descr = (dictionary["description"] as? String)!
        self.rating = dictionary["rating"] as? String
        self.image_url = dictionary["image_url"] as? String
        self.added_time = dictionary["added_time"] as? String
        self.updated_time = dictionary["updated_time"] as? String
        self.user_clipped = dictionary["user_clipped"] as? Int
        self.clip_count = dictionary["clip_count"] as? Int
        self.board_name = dictionary["board_name"] as? String
        self.first_name = dictionary["first_name"] as? String
        self.last_name = dictionary["last_name"] as? String
        self.category_id = dictionary["category_id"] as? String

    }
}

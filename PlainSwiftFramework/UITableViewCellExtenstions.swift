//
//  File.swift
//  PlainSwiftFramework
//
//  Created by mahesh on 13/04/16.
//  Copyright © 2016 maheshbabu.somineni. All rights reserved.
//

import Foundation
extension UITableViewCell {
    func clearBackgroundColors() {
        
        self.backgroundColor = UIColor.clearColor()
        self.contentView.backgroundColor = UIColor.clearColor()
    }
}
//
//  UIViewExtensions.swift
//  PlainSwiftFramework
//
//  Created by maheshbabu.somineni on 4/15/16.
//  Copyright © 2016 maheshbabu.somineni. All rights reserved.
//

import Foundation
extension UIView {
    
    func addBackground(imageName:String) {
        
        // screen width and height:
        let imageViewBackground = UIImageView(frame: CGRectMake(0, 0, UIScreen.mainScreen().bounds.size.width, UIScreen.mainScreen().bounds.size.height ))
        imageViewBackground.image = UIImage(named:imageName)
        
        // you can change the content mode:
        imageViewBackground.contentMode = UIViewContentMode.ScaleAspectFill
        
        self.addSubview(imageViewBackground)
        self.sendSubviewToBack(imageViewBackground)
        
    }
}

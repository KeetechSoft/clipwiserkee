//
//  InAppPurchaseHandler.swift
//  PlainSwiftFramework
//
//  Created by Development on 12/18/15.
//  Copyright © 2015 maheshbabu.somineni. All rights reserved.
//

import UIKit
import StoreKit
import MBProgressHUD

class InAppPurchaseHandler: NSObject, SKProductsRequestDelegate {

    var productsArray: Array<SKProduct!> = []
    var productIDs: Array<String!> = []
    var delegate:BaseViewController!

     override init() {
        
        productIDs.append(GlobalVariables.inappPurchaseProductId)
    }

    func buyProduct(){
        
        if(!self.productsArray.isEmpty){
            
            //Start showing pregress
            delegate.showProgress()

            let payment = SKPayment(product: self.productsArray[0] as SKProduct)
            SKPaymentQueue.defaultQueue().addPayment(payment)
            SKPaymentQueue.defaultQueue().addTransactionObserver(self)
            
        }
    }
    func restoreProducts(){
        
        SKPaymentQueue.defaultQueue().restoreCompletedTransactions()
        
    }
    func getProductInfo() {
        
        if SKPaymentQueue.canMakePayments() {
            
            let productIdentifiers = NSSet(array: productIDs)
            let productRequest = SKProductsRequest(productIdentifiers: productIdentifiers as! Set<String>)
            
            productRequest.delegate = self
            productRequest.start()
        }
        else {
            
            print("Cannot perform In App Purchases.")
        }
    }
    func productsRequest(request: SKProductsRequest, didReceiveResponse response: SKProductsResponse) {
        
        if response.products.count != 0 {
            
            for product in response.products {
                
                productsArray.append(product)
            }
        }
        else {
            
            print("There are no products.")
        }
    }
    func successfulPurchaseLogic(transaction:SKPaymentTransaction){
        
        if (transaction.payment.productIdentifier == GlobalVariables.inappPurchaseProductId) {
            
            let alertController = UIAlertController(title: GlobalVariables.appName, message:"You has successfully purchased the products." as String, preferredStyle: UIAlertControllerStyle.Alert)
            
            alertController.addAction(UIAlertAction(title: "Ok", style: .Default, handler: { (action: UIAlertAction!) in
                
                //Remove all objects
                GlobalSettings.setIsPurchased(true)
                
            }))
            alertController.addAction(UIAlertAction(title: "Cancel", style: .Cancel, handler: { (action: UIAlertAction!) in
                
                
            }))
            
            delegate.presentViewController(alertController, animated: true, completion: nil)
        }
        
        SKPaymentQueue.defaultQueue().finishTransaction(transaction)
    }
}
extension InAppPurchaseHandler:SKPaymentTransactionObserver{

    func paymentQueue(queue: SKPaymentQueue, updatedDownloads downloads: [SKDownload]) {
        
    }
    func paymentQueue(queue: SKPaymentQueue, restoreCompletedTransactionsFailedWithError error: NSError) {
        
    }
    func paymentQueue(queue: SKPaymentQueue, removedTransactions transactions: [SKPaymentTransaction]) {
        
    }
    func paymentQueue(queue: SKPaymentQueue, updatedTransactions transactions: [SKPaymentTransaction]) {
        
        //Hide progress
        delegate.hideProgress()

        //Transaction logic handler
        for transaction in transactions {
            
            switch transaction.transactionState {
                
            case SKPaymentTransactionState.Purchased:

                self.successfulPurchaseLogic(transaction)
                
            case SKPaymentTransactionState.Failed:
                
                GlobalSettings.setIsPurchased(false)
                SKPaymentQueue.defaultQueue().finishTransaction(transaction)
                
            case SKPaymentTransactionState.Restored:
                
                self.successfulPurchaseLogic(transaction)
                
            default:
                break
            }
        }
    }
}

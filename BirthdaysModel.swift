//
//  BirthdaysModel.swift
//  
//
//  Created by Development on 4/14/16.
//
//

import Foundation
import CoreData


class BirthdaysModel: NSManagedObject {

// Insert code here to add functionality to your managed object subclass
    // Insert code here to add functionality to your managed object subclass
    class func insertObject(dictionary:AnyObject, context:NSManagedObjectContext) {
        
        // Create Managed Object
        let entityDescription = NSEntityDescription.entityForName(GlobalVariables.CoreDataEntities.BirthdaysModel.rawValue as String, inManagedObjectContext: context)
        
        //Create new entity
        let object:BirthdaysModel = NSManagedObject(entity: entityDescription!, insertIntoManagedObjectContext: context) as! BirthdaysModel
        
        //Set propert values
        if let value = dictionary.valueForKey("dateOfBirth"){
            
            object.dateOfBirth = value as? String
        }
        if let checked = dictionary.objectForKey("checked") as? NSNumber{
            
            object.checked = checked
        }
        if let value = dictionary.valueForKey("targetDate"){
            
            object.targetDate = value as? String
        }
        if let value = dictionary.valueForKey("nextBirthdayDate"){
            
            object.nextBirthdayDate = value as? String
        }
        if let value = dictionary.valueForKey("age"){
            
            object.age = value as? String
        }
        if let value = dictionary.valueForKey("years"){
            
            object.years = value as? String
        }
        if let value = dictionary.valueForKey("months"){
            
            object.months = value as? String
        }
        if let value = dictionary.valueForKey("weeks"){
            
            object.weeks = value as? String
        }
        if let value = dictionary.valueForKey("days"){
            
            object.days = value as? String
        }
        if let value = dictionary.valueForKey("hours"){
            
            object.hours = value as? String
        }
        if let value = dictionary.valueForKey("minutes"){
            
            object.minutes = value as? String
        }
        if let value = dictionary.valueForKey("seconds"){
            
            object.seconds = value as? String
        }
        if let value = dictionary.valueForKey("name"){
            
            object.name = value as? String
        }
        if let value = dictionary.valueForKey("savedDate"){
            
            object.savedDate = value as? String
        }
        object.uuid = GlobalSettings.getUUID()
        
        //Save the object
        do {
            
            try object.managedObjectContext?.save()
            
            //Add the notification for the new record
            //NotificationsHandler.addNotification(dictionary as AnyObject)
            
        } catch {
            
            print(error)
        }
        
    }
    class func deleteObject() {
        
        
    }
    class func deleteSelectedObjects(context:NSManagedObjectContext) {
        
        //Predicate
        let predicate = NSPredicate(format: "checked = \(NSNumber(bool: true))")
        
        // Initialize Fetch Request
        let fetchRequest = NSFetchRequest()
        
        // Create Entity Description
        let entityDescription = NSEntityDescription.entityForName(GlobalVariables.CoreDataEntities.BirthdaysModel.rawValue as String, inManagedObjectContext: context)
        
        // Configure Fetch Request
        fetchRequest.entity = entityDescription
        fetchRequest.predicate = predicate
        
        do {
            
            let result = try context.executeFetchRequest(fetchRequest)
            
            for object in result{
                
                //Delete the notification for the new record
                //NotificationsHandler.cancelNotification((object.valueForKey("uuid") as? String)!)
                
                context.deleteObject(object as! NSManagedObject)
                
            }
            
        } catch {
            
            print(error as NSError)
        }
        
    }
    class func updateObject(object:BirthdaysModel, dictionary:AnyObject, context:NSManagedObjectContext) {
        
        //Set propert values
        if let value = dictionary.valueForKey("dateOfBirth"){
            
            object.dateOfBirth = value as? String
        }
        if let value = dictionary.valueForKey("targetDate"){
            
            object.targetDate = value as? String
        }
        if let value = dictionary.valueForKey("nextBirthdayDate"){
            
            object.nextBirthdayDate = value as? String
        }
        if let checked = dictionary.objectForKey("checked") as? NSNumber{
            
            object.checked = checked
        }
        if let value = dictionary.valueForKey("age"){
            
            object.age = value as? String
        }
        if let value = dictionary.valueForKey("years"){
            
            object.years = value as? String
        }
        if let value = dictionary.valueForKey("months"){
            
            object.months = value as? String
        }
        if let value = dictionary.valueForKey("weeks"){
            
            object.weeks = value as? String
        }
        if let value = dictionary.valueForKey("days"){
            
            object.days = value as? String
        }
        if let value = dictionary.valueForKey("hours"){
            
            object.hours = value as? String
        }
        if let value = dictionary.valueForKey("minutes"){
            
            object.minutes = value as? String
        }
        if let value = dictionary.valueForKey("seconds"){
            
            object.seconds = value as? String
        }
        
        //Save the object
        do {
            
            //NotificationsHandler.updateNotification(dictionary as AnyObject)
            try object.managedObjectContext?.save()
            
        } catch {
            
            print(error)
        }
        
    }
    class func truncateAllObjects(context:NSManagedObjectContext) {
        
        CommonModel.truncateAllObjects(GlobalVariables.CoreDataEntities.BirthdaysModel.rawValue as String, context:context)
        //NotificationsHandler.cancelAllNotifications()
    }
    class func fetchAllObjects(context:NSManagedObjectContext) -> NSArray {
        
        let resultArray = CommonModel.fetchAllObjects(GlobalVariables.CoreDataEntities.BirthdaysModel.rawValue as String, context: context)
        
        return resultArray
    }
}
